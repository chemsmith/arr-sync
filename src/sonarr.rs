use pbws::{Request, Response};
use serde::{Deserialize, Serialize};

use crate::sync::{sync_media, MediaType, PirateEvent};

#[derive(Serialize, Deserialize, Debug)]
enum PayloadType {
    Test,
    Download,
}

#[derive(Serialize, Deserialize, Debug)]
struct PartialPayload {
    #[serde(rename = "eventType")]
    event_type: PayloadType,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct WebhookSeries {
    title: String,
    path: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct WebhookEpisodeFile {
    relative_path: String,
    path: String,
    scene_name: String,
    size: i64,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct OnImportPayload {
    series: WebhookSeries,
    episode_file: WebhookEpisodeFile,
    #[serde(default)]
    is_upgrade: bool,
    #[serde(default)]
    deleted_files: Vec<WebhookEpisodeFile>,
}

impl PirateEvent for OnImportPayload {
    fn get_folder(&self) -> String {
        self.series.path.split('/').last().unwrap().to_string()
    }

    fn get_full_path(&self) -> String {
        format!("{}/{}", self.series.path, self.episode_file.relative_path)
    }

    fn get_file_name(&self) -> String {
        self.episode_file.relative_path.clone()
    }

    fn get_old_filenames(&self) -> Vec<String> {
        self.deleted_files
            .iter()
            .map(|f| f.relative_path.clone())
            .collect()
    }

    fn has_old_media(&self) -> bool {
        !self.deleted_files.is_empty()
    }

    fn get_media_type(&self) -> MediaType {
        MediaType::Television
    }
}

pub async fn sonarr_webhook_handler(req: Request) -> Result<Response, Box<dyn std::error::Error>> {
    println!(
        "Sonarr webhook received: {:?}",(
        String::from_utf8_lossy(req.body()))
    );

    let payload_type = serde_json::from_slice::<PartialPayload>(req.body()).unwrap_or(PartialPayload { event_type: PayloadType::Test });

    if let PayloadType::Download = payload_type.event_type {
        match serde_json::from_slice::<OnImportPayload>(req.body()) {
            Ok(webhook) => {
                tokio::spawn(async move {
                    sync_media(&webhook).await;
                });
            }
            Err(e) => {
                println!("Error: {:?}", e);
            }
        }
    }

    Ok(Response::new())
}
