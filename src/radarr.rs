use pbws::{Request, Response};
use serde::{Deserialize, Serialize};

use crate::sync::{sync_media, MediaType, PirateEvent};

#[derive(Serialize, Deserialize, Debug)]
enum PayloadType {
    Test,
    Download,
}

#[derive(Serialize, Deserialize, Debug)]
struct PartialPayload {
    #[serde(rename = "eventType")]
    event_type: PayloadType,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct WebhookMovie {
    title: String,
    folder_path: String,
    year: u16,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct WebhookMovieFile {
    relative_path: String,
    path: String,
    scene_name: String,
    size: i64,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct OnImportPayload {
    movie: WebhookMovie,
    movie_file: WebhookMovieFile,
    #[serde(default)]
    is_upgrade: bool,
    #[serde(default)]
    deleted_files: Vec<WebhookMovieFile>,
}

impl PirateEvent for OnImportPayload {
    fn get_folder(&self) -> String {
        self.movie.folder_path.split('/').last().unwrap().to_string()
    }

    fn get_full_path(&self) -> String {
        format!("{}/{}", self.movie.folder_path, self.movie_file.relative_path)
    }

    fn get_file_name(&self) -> String {
        self.movie_file.relative_path.clone()
    }

    fn get_old_filenames(&self) -> Vec<String> {
        self.deleted_files
            .iter()
            .map(|f| f.relative_path.clone())
            .collect()
    }

    fn has_old_media(&self) -> bool {
        !self.deleted_files.is_empty()
    }

    fn get_media_type(&self) -> MediaType {
        MediaType::Movie
    }
}

pub async fn radarr_webhook_handler(req: Request) -> Result<Response, Box<dyn std::error::Error>> {
    println!(
        "Radarr webhook received: {:?}",(
        String::from_utf8_lossy(req.body()))
    );

    let payload_type = serde_json::from_slice::<PartialPayload>(req.body()).unwrap_or(PartialPayload { event_type: PayloadType::Test });

    if let PayloadType::Download = payload_type.event_type {
        match serde_json::from_slice::<OnImportPayload>(req.body()) {
            Ok(webhook) => {
                tokio::spawn(async move {
                    sync_media(&webhook).await;
                });
            }
            Err(e) => {
                println!("Error: {:?}", e);
            }
        }
    }

    Ok(Response::new())
}
