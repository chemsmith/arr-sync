use once_cell::sync::Lazy;
use tokio::io::AsyncWriteExt;
use std::{env, path, borrow::BorrowMut};

pub struct Config {
    local_television_path: String,
    local_movie_path: String,
    remote_root_path: String,
    http_download_prefix: String,
}

impl Config {
    fn new() -> Config {
        let fetch_environment_variable = |name: &str| -> String {
            match env::var(name) {
                Ok(val) => val,
                Err(e) => panic!("{}: {}", e, name),
            }
        };

        let local_television_path = fetch_environment_variable("LOCAL_TELEVISION_PATH");
        let local_movie_path = fetch_environment_variable("LOCAL_MOVIE_PATH");
        let remote_root_path = fetch_environment_variable("HTTP_REMOTE_ROOT_PATH");
        let http_download_prefix = fetch_environment_variable("HTTP_DOWNLOAD_PREFIX");

        if local_television_path.ends_with(path::MAIN_SEPARATOR) {
            panic!("LOCAL_TELEVISION_PATH must not end with a path separator");
        }

        if local_movie_path.ends_with(path::MAIN_SEPARATOR) {
            panic!("LOCAL_MOVIE_PATH must not end with a path separator");
        }

        if remote_root_path.ends_with(path::MAIN_SEPARATOR) {
            panic!("HTTP_REMOTE_ROOT_PATH must not end with a path separator");
        }

        Config {
            local_television_path,
            local_movie_path,
            remote_root_path,
            http_download_prefix,
        }
    }

    fn get_local_path(&self, media_type: &MediaType) -> &String {
        match media_type {
            MediaType::Television => &self.local_television_path,
            MediaType::Movie => &self.local_movie_path,
        }
    }
}

#[allow(clippy::redundant_closure)]
static CONFIG: Lazy<Config> = Lazy::new(|| Config::new());

fn get_config() -> &'static Config {
    &CONFIG
}

pub enum MediaType {
    Television,
    Movie,
}

pub trait PirateEvent {
    fn get_full_path(&self) -> String;
    fn get_folder(&self) -> String;
    fn get_file_name(&self) -> String;
    fn get_old_filenames(&self) -> Vec<String>;
    fn has_old_media(&self) -> bool;
    fn get_media_type(&self) -> MediaType;
}

pub async fn sync_media<T: PirateEvent>(media: &T) {
    match download_media(media).await {
        Ok(_) => println!("Downloaded media: {}", media.get_full_path()),
        Err(err) => {
            println!("Error when syncing \"{}\": {}", media.get_file_name(), err);
            
            return;
        }
    }

    if media.has_old_media() {
        match remove_old_media(media).await {
            Ok(_) => println!("Removed old media: {}", media.get_full_path()),
            Err(err) => {
                println!("Error when removing old media \"{}\": {}", media.get_file_name(), err);
            }
        }
    }
}

async fn download_media<T: PirateEvent>(media: &T) -> Result<(), Box<dyn std::error::Error>> {
    let remote_path = format!(
        "{}/{}",
        get_config().http_download_prefix,
        media.get_full_path().replace(&get_config().remote_root_path, "")
    );

    let local_dir_path = format!(
        "{}/{}",
        get_config().get_local_path(&media.get_media_type()),
        media.get_folder()
    );

    let local_path = format!("{}/{}", local_dir_path, media.get_file_name());

    tokio::fs::create_dir_all(local_dir_path).await?;

    if tokio::fs::metadata(&local_path).await.is_ok() {
        println!("Removing existing file: {}", local_path);
        tokio::fs::remove_file(&local_path).await?;
    }

    let mut file = match tokio::fs::File::create(&local_path).await {
        Ok(file) => file,
        Err(err) => {
            return Err(Box::new(err));
        }
    };

    println!("Downloading file: {}", media.get_file_name());
    let mut r = match reqwest::get(&remote_path).await {
        Ok(response) => response,
        Err(err) => {
            tokio::fs::remove_file(&local_path).await.unwrap();

            return Err(Box::new(err));
        }
    };

    while let Some(mut chunk) = r.chunk().await? {
        file.write_all_buf(chunk.borrow_mut()).await?;
    }

    Ok(())
}

async fn remove_old_media<T: PirateEvent>(media: &T) -> Result<(), Box<dyn std::error::Error>> {
    for old_file in media.get_old_filenames() {
        let local_path = format!(
            "{}/{}/{}",
            get_config().get_local_path(&media.get_media_type()),
            media.get_folder(),
            old_file
        );

        if tokio::fs::metadata(&local_path).await.is_ok() {
            tokio::fs::remove_file(&local_path).await?;
        }
    }

    Ok(())
}

pub fn init() {
    let config = get_config();

    println!("Local television path: {}", config.local_television_path);
    println!("Local movie path: {}", config.local_movie_path);
    println!("Remote root path: {}", config.remote_root_path);
    println!("HTTP download prefix: {}", config.http_download_prefix);
}
