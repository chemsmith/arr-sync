mod radarr;
mod sonarr;
mod sync;

use crate::radarr::radarr_webhook_handler;
use crate::sonarr::sonarr_webhook_handler;
use crate::sync::init as sync_init;
use pbws::ServerBuilder;

#[tokio::main]
async fn main() {
    let server = ServerBuilder::new()
        .add_route("POST", "/radarr", radarr_webhook_handler)
        .add_route("POST", "/sonarr", sonarr_webhook_handler)
        .build("0.0.0.0", 8080)
        .unwrap();

    sync_init();

    let _ = server.run().await;
}
