FROM rust:slim-bullseye as build

WORKDIR /app

RUN apt update && apt install -yqq git pkg-config libssl-dev

COPY Cargo.toml Cargo.lock .
COPY src src

RUN cargo build --release

FROM debian:bullseye-slim

COPY --from=build /app/target/release/arr-sync /arr-sync

RUN sh -c "\
    export DEBIAN_FRONTEND=noninteractive && \
    apt update && \
    apt install -yqq ca-certificates && \
    update-ca-certificates && \
    apt clean && \
    rm -rf /var/lib/apt/lists/* \
    "

CMD ["/arr-sync"]
